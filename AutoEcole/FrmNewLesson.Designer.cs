﻿namespace AutoEcole
{
    partial class FrmNewLesson
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbCredit = new System.Windows.Forms.TextBox();
            this.eLEVEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.autoEcoleDataSet = new AutoEcole.AutoEcoleDataSet();
            this.cmbNom = new System.Windows.Forms.ComboBox();
            this.cmbHeure = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.cmbVehicule = new System.Windows.Forms.ComboBox();
            this.btnVehicule = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.eLEVETableAdapter = new AutoEcole.AutoEcoleDataSetTableAdapters.ELEVETableAdapter();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.vehiculesDisponiblesTableAdapter1 = new AutoEcole.AutoEcoleDataSetTableAdapters.VehiculesDisponiblesTableAdapter();
            this.queriesTableAdapter1 = new AutoEcole.AutoEcoleDataSetTableAdapters.QueriesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.eLEVEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoEcoleDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Eleve";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(224, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Credit Horaire";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(224, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Heure";
            // 
            // cmbCredit
            // 
            this.cmbCredit.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.eLEVEBindingSource, "creditHoraire", true));
            this.cmbCredit.Location = new System.Drawing.Point(313, 41);
            this.cmbCredit.Name = "cmbCredit";
            this.cmbCredit.Size = new System.Drawing.Size(45, 20);
            this.cmbCredit.TabIndex = 3;
            // 
            // eLEVEBindingSource
            // 
            this.eLEVEBindingSource.DataMember = "ELEVE";
            this.eLEVEBindingSource.DataSource = this.autoEcoleDataSet;
            // 
            // autoEcoleDataSet
            // 
            this.autoEcoleDataSet.DataSetName = "AutoEcoleDataSet";
            this.autoEcoleDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cmbNom
            // 
            this.cmbNom.DataSource = this.eLEVEBindingSource;
            this.cmbNom.DisplayMember = "nom";
            this.cmbNom.FormattingEnabled = true;
            this.cmbNom.Location = new System.Drawing.Point(99, 41);
            this.cmbNom.Name = "cmbNom";
            this.cmbNom.Size = new System.Drawing.Size(119, 21);
            this.cmbNom.TabIndex = 4;
            this.cmbNom.ValueMember = "code";
            // 
            // cmbHeure
            // 
            this.cmbHeure.FormattingEnabled = true;
            this.cmbHeure.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.cmbHeure.Location = new System.Drawing.Point(301, 91);
            this.cmbHeure.Name = "cmbHeure";
            this.cmbHeure.Size = new System.Drawing.Size(57, 21);
            this.cmbHeure.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Location = new System.Drawing.Point(45, 141);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(136, 55);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "durée";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(74, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(42, 17);
            this.radioButton2.TabIndex = 9;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "2 H";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(42, 17);
            this.radioButton1.TabIndex = 8;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "1 H";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // cmbVehicule
            // 
            this.cmbVehicule.FormattingEnabled = true;
            this.cmbVehicule.Location = new System.Drawing.Point(45, 223);
            this.cmbVehicule.Name = "cmbVehicule";
            this.cmbVehicule.Size = new System.Drawing.Size(136, 21);
            this.cmbVehicule.TabIndex = 8;
            // 
            // btnVehicule
            // 
            this.btnVehicule.Location = new System.Drawing.Point(275, 215);
            this.btnVehicule.Name = "btnVehicule";
            this.btnVehicule.Size = new System.Drawing.Size(83, 34);
            this.btnVehicule.TabIndex = 9;
            this.btnVehicule.Text = "Vehicules disponibles";
            this.btnVehicule.UseVisualStyleBackColor = true;
            this.btnVehicule.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(45, 278);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(136, 34);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Enregistrer";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // eLEVETableAdapter
            // 
            this.eLEVETableAdapter.ClearBeforeFill = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(45, 96);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(173, 20);
            this.dateTimePicker1.TabIndex = 11;
            this.dateTimePicker1.Value = new System.DateTime(2017, 9, 20, 0, 0, 0, 0);
            // 
            // vehiculesDisponiblesTableAdapter1
            // 
            this.vehiculesDisponiblesTableAdapter1.ClearBeforeFill = true;
            // 
            // FrmNewLesson
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 324);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnVehicule);
            this.Controls.Add(this.cmbVehicule);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmbHeure);
            this.Controls.Add(this.cmbNom);
            this.Controls.Add(this.cmbCredit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmNewLesson";
            this.Text = "FrmNewLesson";
            this.Load += new System.EventHandler(this.FrmNewLesson_Load);
            ((System.ComponentModel.ISupportInitialize)(this.eLEVEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoEcoleDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox cmbCredit;
        private System.Windows.Forms.ComboBox cmbNom;
        private System.Windows.Forms.ComboBox cmbHeure;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.ComboBox cmbVehicule;
        private System.Windows.Forms.Button btnVehicule;
        private System.Windows.Forms.Button btnSave;
        private AutoEcoleDataSet autoEcoleDataSet;
        private System.Windows.Forms.BindingSource eLEVEBindingSource;
        private AutoEcoleDataSetTableAdapters.ELEVETableAdapter eLEVETableAdapter;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private AutoEcoleDataSetTableAdapters.VehiculesDisponiblesTableAdapter vehiculesDisponiblesTableAdapter1;
        private AutoEcoleDataSetTableAdapters.QueriesTableAdapter queriesTableAdapter1;
    }
}