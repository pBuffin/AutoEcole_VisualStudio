﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoEcole
{
    public partial class FrmEleve : Form
    {
        public FrmEleve()
        {
            InitializeComponent();
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            txtDate.Text = monthCalendar1.SelectionStart.ToShortDateString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nom = txtNom.Text;
            DateTime? dt = Convert.ToDateTime(txtDate.Text); string prenom = txtPrenom.Text;
            int? forfait = Convert.ToInt32(cmbForfait.SelectedItem); string adresse = txtAdresse.Text;
            try
            {
                queriesTableAdapter1.pEleve_INSERT(nom, dt, prenom, adresse, forfait);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FrmEleve_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'autoEcoleDataSet.ELEVE'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.eLEVETableAdapter.Fill(this.autoEcoleDataSet.ELEVE);

        }

        private void bindingNavigatorMovePreviousItem_Click(object sender, EventArgs e)
        {

        }
    }
}
