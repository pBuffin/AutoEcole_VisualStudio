﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoEcole
{
    public partial class FrmListeLesson : Form
    {
        public FrmListeLesson()
        {
            InitializeComponent();
        }

        private void FrmListeLesson_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'autoEcoleDataSet.LECON'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.lECONTableAdapter.Fill(this.autoEcoleDataSet.LECON);
            // TODO: cette ligne de code charge les données dans la table 'autoEcoleDataSet.ELEVE'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.eLEVETableAdapter.Fill(this.autoEcoleDataSet.ELEVE);

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                lECONTableAdapter.Update(autoEcoleDataSet.LECON);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
    }

