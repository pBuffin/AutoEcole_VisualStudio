﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoEcole
{
    public partial class FrmNewLesson : Form
    {
        public FrmNewLesson()
        {
            InitializeComponent();
        }

        private void FrmNewLesson_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'autoEcoleDataSet.ELEVE'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.eLEVETableAdapter.Fill(this.autoEcoleDataSet.ELEVE);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int? heure = Convert.ToInt32(cmbHeure.SelectedItem);
            DateTime dateLecon = dateTimePicker1.Value;
            DataTable dt = vehiculesDisponiblesTableAdapter1.GetData(dateLecon, heure); cmbVehicule.DataSource = dt;
            cmbVehicule.DisplayMember = dt.Columns[0].ColumnName;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            short? codeEleve = Convert.ToInt16(cmbNom.SelectedValue); DateTime? date = dateTimePicker1.Value; short? heure = Convert.ToInt16(cmbHeure.Text); short? duree;
            if (radioButton1.Checked)
                duree = 1;
            else duree = 2;
            bool? Effectuee = false;
            string numImma = cmbVehicule.Text;
            queriesTableAdapter1.pLECON_INSERT(date, codeEleve, heure, duree, Effectuee, numImma);
        }
    }
}
