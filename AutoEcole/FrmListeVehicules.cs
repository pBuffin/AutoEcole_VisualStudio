﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoEcole
{
    public partial class FrmListeVehicules : Form
    {
        public FrmListeVehicules()
        {
            InitializeComponent();
        }

        private void FrmListeVehicules_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'autoEcoleDataSet.VEHICULE'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.vEHICULETableAdapter.Fill(this.autoEcoleDataSet.VEHICULE);

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.vEHICULETableAdapter.Update(autoEcoleDataSet.VEHICULE);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            autoEcoleDataSet.VEHICULE.RejectChanges();
        }
    }
    }

