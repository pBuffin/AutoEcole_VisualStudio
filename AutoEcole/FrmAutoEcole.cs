﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoEcole
{
    public partial class FrmAutoEcole : Form
    {
        public FrmAutoEcole()
        {
            InitializeComponent();
        }

      

        private void formulaireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmVehicule frmVehicule = new FrmVehicule();
            frmVehicule.Show();
        }

        private void listeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmListeVehicules frmListeVehicules = new FrmListeVehicules();
            frmListeVehicules.Show();
        }

        private void elèveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEleve frmEleve = new FrmEleve();
            frmEleve.Show();
        }

        private void nouvelleLeçonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmNewLesson frmNewLesson = new FrmNewLesson();
            frmNewLesson.Show();
        }

        private void listeLeçconToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmListeLesson frmListeLesson = new FrmListeLesson();
            frmListeLesson.Show();
        }
    }
}
