﻿namespace AutoEcole
{
    partial class FrmAutoEcole
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuGestion = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elèveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vehiculeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formulaireToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leçonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouvelleLeçonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeLeçconToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuGestion.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuGestion
            // 
            this.menuGestion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.elèveToolStripMenuItem,
            this.vehiculeToolStripMenuItem,
            this.leçonToolStripMenuItem});
            this.menuGestion.Location = new System.Drawing.Point(0, 0);
            this.menuGestion.Name = "menuGestion";
            this.menuGestion.Size = new System.Drawing.Size(463, 24);
            this.menuGestion.TabIndex = 0;
            this.menuGestion.Text = "menuGestion";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // elèveToolStripMenuItem
            // 
            this.elèveToolStripMenuItem.Name = "elèveToolStripMenuItem";
            this.elèveToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.elèveToolStripMenuItem.Text = "Elève";
            this.elèveToolStripMenuItem.Click += new System.EventHandler(this.elèveToolStripMenuItem_Click);
            // 
            // vehiculeToolStripMenuItem
            // 
            this.vehiculeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formulaireToolStripMenuItem,
            this.listeToolStripMenuItem});
            this.vehiculeToolStripMenuItem.Name = "vehiculeToolStripMenuItem";
            this.vehiculeToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.vehiculeToolStripMenuItem.Text = "Vehicule";
            // 
            // formulaireToolStripMenuItem
            // 
            this.formulaireToolStripMenuItem.Name = "formulaireToolStripMenuItem";
            this.formulaireToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.formulaireToolStripMenuItem.Text = "Formulaire";
            this.formulaireToolStripMenuItem.Click += new System.EventHandler(this.formulaireToolStripMenuItem_Click);
            // 
            // listeToolStripMenuItem
            // 
            this.listeToolStripMenuItem.Name = "listeToolStripMenuItem";
            this.listeToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.listeToolStripMenuItem.Text = "Liste";
            this.listeToolStripMenuItem.Click += new System.EventHandler(this.listeToolStripMenuItem_Click);
            // 
            // leçonToolStripMenuItem
            // 
            this.leçonToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouvelleLeçonToolStripMenuItem,
            this.listeLeçconToolStripMenuItem});
            this.leçonToolStripMenuItem.Name = "leçonToolStripMenuItem";
            this.leçonToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.leçonToolStripMenuItem.Text = "Leçon";
            // 
            // nouvelleLeçonToolStripMenuItem
            // 
            this.nouvelleLeçonToolStripMenuItem.Name = "nouvelleLeçonToolStripMenuItem";
            this.nouvelleLeçonToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.nouvelleLeçonToolStripMenuItem.Text = "Nouvelle Leçon";
            this.nouvelleLeçonToolStripMenuItem.Click += new System.EventHandler(this.nouvelleLeçonToolStripMenuItem_Click);
            // 
            // listeLeçconToolStripMenuItem
            // 
            this.listeLeçconToolStripMenuItem.Name = "listeLeçconToolStripMenuItem";
            this.listeLeçconToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.listeLeçconToolStripMenuItem.Text = "Liste Leçon";
            this.listeLeçconToolStripMenuItem.Click += new System.EventHandler(this.listeLeçconToolStripMenuItem_Click);
            // 
            // FrmAutoEcole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 261);
            this.Controls.Add(this.menuGestion);
            this.MainMenuStrip = this.menuGestion;
            this.Name = "FrmAutoEcole";
            this.Text = "AutoEcole";
            this.menuGestion.ResumeLayout(false);
            this.menuGestion.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuGestion;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elèveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vehiculeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leçonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formulaireToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouvelleLeçonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeLeçconToolStripMenuItem;
    }
}

