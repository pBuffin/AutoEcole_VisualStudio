﻿namespace AutoEcole
{
    partial class FrmListeVehicules
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvListeVehicule = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.autoEcoleDataSet = new AutoEcole.AutoEcoleDataSet();
            this.vEHICULEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vEHICULETableAdapter = new AutoEcole.AutoEcoleDataSetTableAdapters.VEHICULETableAdapter();
            this.numImmaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modeleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.couleurDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListeVehicule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoEcoleDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vEHICULEBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvListeVehicule
            // 
            this.dgvListeVehicule.AutoGenerateColumns = false;
            this.dgvListeVehicule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListeVehicule.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numImmaDataGridViewTextBoxColumn,
            this.modeleDataGridViewTextBoxColumn,
            this.couleurDataGridViewTextBoxColumn});
            this.dgvListeVehicule.DataSource = this.vEHICULEBindingSource;
            this.dgvListeVehicule.Location = new System.Drawing.Point(26, 12);
            this.dgvListeVehicule.Name = "dgvListeVehicule";
            this.dgvListeVehicule.Size = new System.Drawing.Size(344, 200);
            this.dgvListeVehicule.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(54, 238);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 34);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Sauvegarder";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(215, 238);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(107, 34);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Annuler";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // autoEcoleDataSet
            // 
            this.autoEcoleDataSet.DataSetName = "AutoEcoleDataSet";
            this.autoEcoleDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vEHICULEBindingSource
            // 
            this.vEHICULEBindingSource.DataMember = "VEHICULE";
            this.vEHICULEBindingSource.DataSource = this.autoEcoleDataSet;
            // 
            // vEHICULETableAdapter
            // 
            this.vEHICULETableAdapter.ClearBeforeFill = true;
            // 
            // numImmaDataGridViewTextBoxColumn
            // 
            this.numImmaDataGridViewTextBoxColumn.DataPropertyName = "numImma";
            this.numImmaDataGridViewTextBoxColumn.HeaderText = "numImma";
            this.numImmaDataGridViewTextBoxColumn.Name = "numImmaDataGridViewTextBoxColumn";
            // 
            // modeleDataGridViewTextBoxColumn
            // 
            this.modeleDataGridViewTextBoxColumn.DataPropertyName = "modele";
            this.modeleDataGridViewTextBoxColumn.HeaderText = "modele";
            this.modeleDataGridViewTextBoxColumn.Name = "modeleDataGridViewTextBoxColumn";
            // 
            // couleurDataGridViewTextBoxColumn
            // 
            this.couleurDataGridViewTextBoxColumn.DataPropertyName = "couleur";
            this.couleurDataGridViewTextBoxColumn.HeaderText = "couleur";
            this.couleurDataGridViewTextBoxColumn.Name = "couleurDataGridViewTextBoxColumn";
            // 
            // FrmListeVehicules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 303);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dgvListeVehicule);
            this.Name = "FrmListeVehicules";
            this.Text = "Liste des Vehicules";
            this.Load += new System.EventHandler(this.FrmListeVehicules_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListeVehicule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.autoEcoleDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vEHICULEBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvListeVehicule;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private AutoEcoleDataSet autoEcoleDataSet;
        private System.Windows.Forms.BindingSource vEHICULEBindingSource;
        private AutoEcoleDataSetTableAdapters.VEHICULETableAdapter vEHICULETableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn numImmaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modeleDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn couleurDataGridViewTextBoxColumn;
    }
}