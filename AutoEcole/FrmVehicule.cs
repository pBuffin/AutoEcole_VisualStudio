﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoEcole
{
    public partial class FrmVehicule : Form
    {
        public FrmVehicule()
        {
            InitializeComponent();
            vehiculeTableAdapter1.Fill(monDS.VEHICULE);
        }



        private void tsbSave_Click(object sender, EventArgs e)
        {
            try
            {
                bsVehicule.EndEdit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                bsVehicule.CancelEdit();
            }
            try
            {
                vehiculeTableAdapter1.Update(monDS.VEHICULE);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
    
 


