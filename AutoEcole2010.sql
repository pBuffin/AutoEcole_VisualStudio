if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_LECON_ELEVE]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[LECON] DROP CONSTRAINT FK_LECON_ELEVE
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_LECON_VEHICULE]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[LECON] DROP CONSTRAINT FK_LECON_VEHICULE
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[pEleve_INSERT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[pEleve_INSERT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ELEVE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ELEVE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LECON]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[LECON]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VEHICULE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VEHICULE]
GO

CREATE TABLE [dbo].[ELEVE] (
	[code] [smallint] NOT NULL ,
	[nom] [nvarchar] (20) COLLATE French_CI_AS NOT NULL ,
	[dateInscription] [smalldatetime] NOT NULL ,
	[prenom] [nvarchar] (15) COLLATE French_CI_AS NOT NULL ,
	[adresse] [nvarchar] (30) COLLATE French_CI_AS NOT NULL ,
	[creditHoraire] [int] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[LECON] (
	[numero] [int] NOT NULL ,
	[date] [smalldatetime] NOT NULL ,
	[codeEleve] [smallint] NULL ,
	[heure] [smallint] NULL ,
	[duree] [smallint] NULL ,
	[effectuee] [bit] NOT NULL ,
	[numImmaVehicule] [nvarchar] (8) COLLATE French_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[VEHICULE] (
	[numImma] [nvarchar] (8) COLLATE French_CI_AS NOT NULL ,
	[modele] [nvarchar] (15) COLLATE French_CI_AS NULL ,
	[couleur] [nvarchar] (50) COLLATE French_CI_AS NULL 
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
ALTER TABLE [dbo].[ELEVE] WITH NOCHECK ADD 
	CONSTRAINT [PK_ELEVE] PRIMARY KEY  CLUSTERED 
	(
		[code]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[VEHICULE] WITH NOCHECK ADD 
	CONSTRAINT [PK_VEHICULE] PRIMARY KEY  CLUSTERED 
	(
		[numImma]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[LECON] WITH NOCHECK ADD 
	CONSTRAINT [PK_LECON] PRIMARY KEY  CLUSTERED 
	(
		[numero]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[LECON] ADD 
	CONSTRAINT [FK_LECON_ELEVE] FOREIGN KEY 
	(
		[codeEleve]
	) REFERENCES [dbo].[ELEVE] (
		[code]
	),
	CONSTRAINT [FK_LECON_VEHICULE] FOREIGN KEY 
	(
		[numImmaVehicule]
	) REFERENCES [dbo].[VEHICULE] (
		[numImma]
	)
GO

/****** Objet :  Procédure stockée dbo.pEleve_INSERT   */ 
CREATE PROC pEleve_INSERT
     @nom nvarchar(8)
    ,@dateInscription smallDateTime
    ,@prenom nvarchar(15)
    ,@adresse nvarchar(30)
    ,@creditHoraire int
AS
Declare @code smallint
SELECT @code = (select max(code) from eleve)
select @code = (@code+1)
INSERT eleve (
     code
    ,nom
    ,dateInscription
    ,prenom
    ,adresse
    ,creditHoraire
) 
VALUES (
     @code
    ,@nom
    ,@dateInscription
    ,@prenom
    ,@adresse
    ,@creditHoraire
)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

